;; 2 Lisp Programming Types

;; 2.3.7 Array Type
;; String Type

"this is a string"

;; espace the " with \
"\""

;; new line are included in the text
"there is a newline
here"

;; escape newline with \
"there is a newline \
here"

;; non-ASCII chars are represented by multibyte and unibyte
;; unibyte stores one char in one byte, value will be between 0 - 255
;; multibyte stores one char in multiple bytes

;; they could write literally in source code
"你好"
"አማርኛ"

;; or, use escaping sequence
;; in hexadecimal escape
"\x41"
;;; => "A"
"\xe0"
;;; => "à"

;; in octal escaping sequence
"\101"
;;; => "A"

"\001"
;;; => ""

;; in unicode escaping sequence, this will infer a multibyte string
"\u1000"
;;; => "က"

;; `\ ' will terminate a escaping if necessary
"\xe0\ "

;; the same backslash escaping-sequence for characters could be used in a string
;; for valid ASCII control characters
"\C-a"

;; for meta characters, it sets the 2^7 bit on the character of string
;; and will translate to the equivalent meta character if necessary
;; the print value will be the same
"\M-a"

;; syntax to include the text properties for a string
;; properties are specified in a group of three BEG END PLIST
#("foo bar" 0 3 (face bold) 3 4 nil 4 7 (face italic))


;; 2.3.9 Vector Type
;; vector has O(1) access, they could hold elements of any type
[1 "two" (three)]

;; 2.3.10 Char-Table Type
;; a fixed-length array indexed by character codes
;; they could link to a parent, default value, have special slots

;; 2.3.11 Bool-Vector Type
;; a one-dimensional array only holds `t' and `nil'
;; syntax uses #&<length> as a prefix for the string
;; the string stands for the bitmap,
;; extra bits other than the length are omitted

(make-bool-vector 3 t)

"\017" #&3"\007")
;;; => t

(equal #&4"\017" #&4"\007")
;;; => nil

;; 2.3.12 Hash Table Type
(make-hash-table)

;; 2.3.13 Function Type
;; function is created by `lambda'
(lambda (x) (+ x x))

;; `symbol-function' returns SYMBOL's function definition, or nil if that is void.
(symbol-function 'find-function)

;; primitive functions are defined in C, they have no read syntax
;; they are also called "subrs" for "subroutine"
(symbol-function 'car)

;; function might also be byte-complied, byte-code function is printed as #[...]


;; 2.3.14 Macro Type
;; any list car is `macro' and cdr is a lisp function object ?
;; usually defined with `defmacro'

;; 2.3.17 Autoload Type
;; (autoload ... ) defines an autoload object
;; they are used to defer the file loading until a function is referenced
