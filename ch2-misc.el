;; #N= together with #N# could define circular objects

(setq x '#1=(a #1#))

(eq x (cadr x))
;;; => t

(setq x '(#1=(a) b #1#))
(eq (nth 0 x) (nth 2 x))
;;; => t

;; there are various type predicates build-in in Elisp
;; most of them ends in *-p

(atom 1)
;;; => t
(atom nil)
;;; => t
(atom 'a)
;;; => t
(atom "a")
;;; => t
(atom '(a))
;;; => nil


;; `type-of' returns the primitive type of one object
(type-of 1)
;;; => integer
(type-of 'nil)
;;; => symbol
(type-of '())
;;; => symbol, '() is 'nil
(type-of '(x))
;;; => cons

;; 2.7 Equality Predicates
;; `eq' tests identities of two objects, this is the most strict comparison
(eq 1 1)
;;; => t
(eq 'a 'a)
;;; => t
(eq '(1 2) '(1 2))
;;; => nil
(eq "asdf" "asdf")
;;; => nil

;; `equal' recursively tests for equal components
(equal '(1 2) '(1 2))
;;; => t
(equal "asdf" "asdf")
;;; => t

;; they ignore text properties in string test
(equal
 #("hello world!" 0 2 (font-info "a"))
 #("hello world!" 4 9 (font-info "b")))
;;; => t

;; use `equal-including-properties' for a stricter test
(equal-including-properties
 #("hello world!" 0 2 (font-info "a"))
 #("hello world!" 4 9 (font-info "b")))
;;; => nil
