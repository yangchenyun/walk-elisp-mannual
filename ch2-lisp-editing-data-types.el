;; 2.4 Editing Types

;; *buffer* is one core data structure. They usually hold content from a *file*,
;; has a designated *position* and could be displayed in a *window*.
;; For each buffer, it contains a *syntax-table*, a *keymap* and
;; *local-variables bindinds*, *overlays* and *text properties*
(current-buffer)

;; *marker* denotes a position in a specific buffer
(point-marker)

;; *window* is the portion of a terminal screen where emacs display a buffer
;; only one window will be the `selected-window'
(selected-window)

;; one or many windows are grouped into frames

;; *frame* is a screen area contains one or more *windows*
(selected-frame)
;; *frame* may contain a "window configuration" about the current state of windows
;; "frame configuration" contains the windows information in all frames

;; *terminal* is a device capable of displaying one or more Emacs frames
(get-device-terminal nil)

;; in ELisp, *process* is an object designated a subprocess created by Emacs process
;; they usually connected with Emacs through textual input/output
(process-list)
;;; => (#<process Python[/tmp/some.py]> #<process server>)

;; many objects could be used as a *stream*
;; nil is a stream for stdin/stdout
;; t is a stream for minibuffer-input/echo-area-output

;; *keymaps* is a list whose car is `keymap'

;; *overlay* displays part of a buffer in different display style

;; *font objects*, *font-specs* and *font-entiries* are used to display in a terminal
