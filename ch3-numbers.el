;;; 3 Numbers

;; 3.1 Integer Basics

;; Elisp supports integers
-3
;;; => -3
1
;;; => 1
+1
;;; => 1
-1
;;; => -1

;; and floating-point numbers
0.0
;;; => 0.0

1.5e2
;;; => 150.0

;; in x86 machine 30 bits are used to store integers
;; (30 bits; i.e., -2**29 to 2**29)

;; in a x64 machine, 61 bits are used
;; (61 bits; i.e., -2**61 to 2**61 - 1)
2305843009213693951
-2305843009213693952

;; they are stored in variables as well
most-positive-fixnum
most-negative-fixnum

;; integer could overflow
(+ 2305843009213693951 1)

;; The syntax for integers in bases other than 10 uses `#' followed by
;; a letter that specifies the radix: `b' for binary, `o' for octal,
;; `x'for hex, or `RADIXr' to specify radix RADIX.

#b101100
;;; => 44
#o54
;;; => 44
#x2c
;;; => 44
#24r1k
;;; => 44


;; 3.2 Floating-Point Basics
;; floating point range is the same as C data type `double'

;; floating point requires a decimal point or exponent
1500.0
+15e2
15.0e+2
+1500000e-3
.15e4

;; one digit after decimal point is required to declare a floating-point number
;; this is not a float
1500.
;;; => 1500

;; 0.0 equals to ordinary 0
(= 0.0 0)
;;; => t

;; but not the with the same type
(equal 0.0 0)
;;; => nil

;; infinity is floating-point number as well
(floatp 1.0e+INF)
;;; => t
(floatp -1.0e+INF)
;;; => t

;; not-a-number is floating-point as well
(/ 0.0 0.0)
;;; => -0.0e+NaN

(floatp 0.0e+NaN)
;;; => t

;; `isnan' to test NaN directly
(isnan 0.0e+NaN)
;;; => t

;; `frexp' returns the significand and exponent of the floating-point
(frexp 100)
;;; => (0.78125 . 7)

;; `ldexp' returns a floating-point number corresponding to the significand SIG and exponent EXP.
(ldexp 0.74 10)
;;; => 757.76

`logb' returns the rounded-down binary exponent of X.
;;; => logb
(logb 12)

;; 3.3 Type Predicates for Numbers
(floatp 10.0)
(integerp 10)
(numberp 10.0)

;; test for natrual number
(natnump -1)

;; `zerop' must take in an number for testing
(zerop 0)

(zerop 'not-number)

;; 3.4 Comparison of Numbers
;; `=' compares by value and should be used in general
;; `eq' only compares object types and could be used in comparision with unknown objects
(= 1 1e0)
;;; => t

(eq 1 1.0)
;;; => nil

;; `eql' compares numbers by type and numeric value
(eq 1.0 1e0)
;;; => nil
(eql 1.0 1e0)
;;; => t

;; comparison functions include `<', `<=', `>', `>='

;; and some basic functions, they return the most loosen type of its arguments
(max 20)
;;; => 20
(max 1 3 2.5)
;;; => 3.0

(min -4 1)
;;; => -4


;; 3.5 Numeric Conversions

;; `float' could be used to converted to floating point
(float 1)
;;; => 1.0

;; `truncate' rounds towards zero for number/divisor
(truncate 11.2)
;;; => 11
(truncate -11.2 3)
;;; => -3

;; `floor' rounds towards -1.0e+INF
(floor -11.2 3)
;;; => -4

;; `ceiling' rounds towards +1.0e+INF
(ceiling 11.2 3)

;; `round' rounds to the nearest integers,
(round 1.2)
;;; => 1
(round 1.7)
;;; => 2
(round -1.2)
;;; => -1

;; there are corresponding rounding takes one floating-point number exist
(ffloor 11.2)
;;; => 11.0

(fceiling 11.2)
;;; => 12.0

(fround 11.5)
;;; => 12.0

(ftruncate -11.5)
;;; => -11.0

;; even number is prefered in a equidistant
(round -1.5)
;;; => -2

;; 3.6 Arithmetic Operations
(1+ 10)
;;; => 11

(1- 12)
;;; => 11

(+ 1 2 3 4)
;;; => 10

(- 10)
;;; => -10

(- 10 5)
;;; => 5

(- 10 1 2 3 4)
;;; => 0

(* 1 2 3 4)
;;; => 24

;; return the least strict data type
(/ -17 6)
;;; => -2

(/ -17 6.0)
;;; => -2.8333333333333335

(/ 35 5 7)
;;; => 1

;; `%' only allows integers
(% 9 4)

;; `mod' allows floating-point, it floors the quotient
(mod 5.5 2.5)

;; 3.8 Bitwise Operations on Integers

;; `lsh' shifts left with padding zeros
(lsh 7 1)
;; Decimal 7 becomes decimal 14.
;; 00000111 => 00001110

;; make negative shift (shift to right)
(lsh 7 -1)
;; 00000111 => 00000011

;; this could cause overflows
(lsh most-positive-fixnum 1)
;;; => -2
;; 011...111 => 111...110

;; `ash' arithmetic shift, will padding with 1 on the left
(ash -6 -1)

;; 1111...111010 (61 bits total)
;; =>
;; 1111...111101 (61 bits total)

(lsh -6 -1)
;; 1111...111010 (61 bits total)
;; =>
;; 0111...111101 (61 bits total)

;; `logand', `logior', `logxor', `lognot' to perform other binary functions
(logand 14 13)
;;; => 12

;; identity is -1, which is all ones in binary
(logand)
;;; => -1

(logior 14 13)
;;; => 15

;;; identity is 0, which all zeros in binary
(logior)
;;; => 0

(logxor 14 13)
;;; => 3

(lognot 14)
;;; -15

;; 3.9 Standard Mathematical Functions
;; mathematical constant
float-e
float-pi

;; `sin', `cos' and `tan' receives arguments in radians
(sin (/ float-pi 4))
;; (/ (sqrt 2) 2) => 0.7071067811865476
(cos float-pi)
(tan (/ float-pi 4))

;; `asin', `acos' and `atan' returns [-pi/2, pi/2]

(asin -1.0)
;;; => -1.5707963267948966 -pi / 2
(atan +1.0e+INF)
;;; => 1.5707963267948966 pi / 2

;; raise e to power
(exp 2)

;; raise any base to power
(expt 2 10)

;; calculate logarithm
(log 1024 2)

;; sqrt
(sqrt 100)

;; 3.10 Random Numbers
;; Elisp provides a pseudo-random function generated from a 'seed'
;; with any given seed, the `random' function always generates the same
;; sequence of numbers.

;; pass in a string to `random' to set the seed
(random "")

;; pass in `t' to choose a new seed
(random t)

;; 2.4 Characters
;; text characters are represented by integers in Elisp. Any integer
;; between zero and the value of `(max-char)',inclusive, is a valid
;; character
